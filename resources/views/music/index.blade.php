@extends('layouts.app')


@section('content')


<div class='container'>

<div class='row'>

<div class='col-md-4 col-md-offset-2'>
@foreach($musics as $music)
<music 
title="{{ $music->title }}" 
link='{{ $music->link }}'
name="{{$music->name}}"
mid='{{ $music->id }}'
likes='{{count($music->likes) }}'
login= {{ Auth::check()}}
></music>
@endforeach
</div>
</div>
</div>


@endsection


@section('css')
.musicPost{
    margin-top:20px;
    border: 1px solid black;
}

.musicPost {
    padding-left: 15px;

}
.likebutton:hover{
    color:red;
}


@endsection