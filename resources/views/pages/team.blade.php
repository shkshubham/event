@extends('layouts.app')

@section('content')
<section id="team" class="text-center section">
    <div class='container'>
        <h2><span class="highlight_secondary">Our</span> Team</h2>

        <h5>
        Our people are our greatest asset and biggest differentiator.<br>They also believe in having a lot of fun along the way.
        </h5>

        <div class="hr big_size_hr invisible_hr">
            <i class="fa fa-"></i>
        </div>
        <div class='row'>
            <div class='col-md-6 col-md-offset-3'>
                <h2>Shubham Shukla</h2><h4>Technical expert of the group and responsible for anything around his online laboratory</h4>  
                <h2>Ashish Bisht</h2><h4>Pied piper of group who is responsible for executing plans and research of laboratory</h4>
            </div>
        </div>
    </div>
</section>
@endsection

@section('css')
h1, h2, h3, h4, h5, h6 {
	font-family: "Open Sans";
	font-weight: 300;
}
h1 {
	font-size: 46px;
}
h2 {
	font-size: 44px;
}
h3 {
	font-size: 32px;
}
h4 {
	font-size: 24px;
}
h5 {
	font-size: 22px;
	line-height: 30px;
}
h6 {
	font-size: 20px;
}
.hr.invisible_hr {
    height: 0;
    padding: 0;
}
.hr.big_size_hr {
    margin: 30px 0;
}
.hr {
    clear: both;
    display: block;
    font-size: 0;
    height: 24px;
    margin: 20px 0;
    overflow: hidden;
    padding: 2px 0;
    position: relative;
    text-align: center;
    width: 100%;
}
.highlight_secondary {
    color: #F24046;
}
body {
	font-family: "Open Sans";
	line-height: 26px;
}
@endsection