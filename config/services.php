<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
    'client_id' => '1649225451755621',
    'client_secret' => '815a6caf5ca89c2356f24768db3bc0c5',
    'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
    'twitter' => [
    'client_id' => 'bW375fRdpElHkYrBGdJAVoeWA',
    'client_secret' => 'KpIAL2z975Obk8Tfxv3ihsaF6AoT38PycCc5MSrrVPDONpoUpd',
    'redirect' => 'http://localhost:8000/login/twitter/callback',
    ],
    'google' => [
    'client_id' => '381307275198-sobq3b6s7ldb1ges98lj24rogo5hsvne.apps.googleusercontent.com',
    'client_secret' => 'NesqGAAQfqsSUTuT1fOebWM0',
    'redirect' => 'http://localhost:8000/login/google/callback',
    ],
];
