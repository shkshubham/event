<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $fillable = [
        'title', 'artist_id','link',
    ];

    protected $hidden = [
        'artist_id',
    ];
    public function artist(){
        return $this->belongsTo('App\Artist');
    }
    public function likes(){
        return $this->hasMany('App\Like');
    }
    
}
