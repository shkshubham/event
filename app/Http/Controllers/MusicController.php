<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Music;
use Auth;
use App\Artist;

class MusicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:artist');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'link' => 'required|email|max:255|unique'
        ]);
    }
    public function index(){
        $musics = Music::with('likes')->get();
        return view('music.index',compact('musics', 'likestatus'));
    }
    public function musiclist(){
        $musics = Music::with('likes')->get();
        return $musics;
    }


    public function create(){
        return view('music.create');
    }
    public function store(Request $request){
            $artistname = Auth::guard('artist')->user()->name;
            $music_file = $request->file('link');
            $filename = $music_file->getClientOriginalName();
            $location = public_path('audio/' . $artistname);
            $music_file->move($location,$filename);
            
            Music::create([
                'artist_id' => Auth::guard('artist')->user()->id,
                'title' => $request->title,
                'link' => $filename
            ]);

    }
    public function test(){
        $artists = Artist::find(1)->music;
        return $artists;
    }

}
