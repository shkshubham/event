<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($service)
    {
        if($service == 'google'){
            $socialUser = Socialite::driver($service)->stateless()->user();
        }else{
            $socialUser = Socialite::driver($service)->user();            
        }
        $finduser = User::where('email',$socialUser->email)->first();
        if($finduser){
            Auth::login($finduser);
            return redirect('/home');
        }
        else{
            $user = new User;
            $user->name = $socialUser->name;
            $user->email = $socialUser->email;
            $user->password = bcrypt('password');
            $user->save();
            Auth::login($user);
            return redirect('/home');
        }

        // $user->token;
    }
}
