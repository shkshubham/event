<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use Auth;
use Artist;

class ArtistLoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:artist');
    }

    public function showLoginForm()
    {
        return view('artist.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        $cred = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if(Auth::guard('artist')->attempt($cred,$request->remember)){
            return redirect()->intended(route('artist.dashboard'));
        }
        return redirect()->back()->withInput($request->only('email'));
    }
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($service)
    {
            
        if($service == 'google'){
            $socialUser = Socialite::driver($service)->stateless()->user();
        }else{
            $socialUser = Socialite::driver($service)->user();            
        }
        $finduser = Artist::where('email',$socialUser->email)->first();
        if($finduser){
            //Auth:guard('fan')->login($finduser);
            return redirect('/artist');
        }
        else{
            $user = new Artist;
            $user->name = $socialUser->name;
            $user->email = $socialUser->email;
            $user->password = bcrypt('password');
            $user->save();
            Auth::guard('artist')->attempt($finduser);
            return redirect()->intended(route('artist.dashboard'));
        }

        // $user->token;
    }

}
