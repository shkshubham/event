<?php

namespace App\Http\Controllers\Artist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArtistHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:artist');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artist.home');
    }
}
