<?php

namespace App\Http\Controllers\Artist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Artist;
use Auth;

class ArtistController extends Controller
{
    public function show($username){
        $artist = Artist::where('username', $username)->firstorFail();
        return view('artist.show',compact('artist'));
    }

    public function edit($username){
        $artist = Artist::where('username', $username)->firstOrFail();
        
                if (Auth::guard('artist')->user()->id == $artist->id) {
                    return view('artist.edit',compact('artist'));
                }
        
                return 'You can not edit your profile';
    }

    public function update(Request $request, $username){
        $artist = Artist::where('username', $username)->firstOrFail();;
        $artist->update($request->all());
        return redirect(route('artist.dashboard'));
    }
}
