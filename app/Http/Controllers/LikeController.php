<?php

namespace App\Http\Controllers;
use App\Like;
use Auth;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function store(Request $request){
        $likecheck = Like::where(['user_id'=>Auth::id(), 'music_id'=>$request->id])->first();
        if($likecheck){
            Like::where(['user_id'=>Auth::id(), 'music_id'=>$request->id])->delete();
            return 'deleted';
        }
        else{
            $like = new Like;
            $like->user_id = Auth::id();
            $like->music_id = $request->id;
            $like->save();
        }
        
    }
}
