<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

//Social Login
Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/team','PagesController@team')->name('team');
Route::get('/about', 'PagesController@about')->name('about');

//Artist
Route::get('/artist/register','Artist\ArtistRegisterController@showRegisterForm')->name('artist.register');
Route::post('/artist/register','Artist\ArtistRegisterController@register')->name('artist.register.submit');
Route::get('/artist/login','Artist\ArtistLoginController@showLoginForm')->name('artist.login');
Route::post('/artist/login','Artist\ArtistLoginController@login')->name('artist.login.submit');
Route::get('/artist','Artist\ArtistHomeController@index')->name('artist.dashboard');

//Artist Profile
Route::get('/artist/profile/{username}', 'Artist\ArtistController@show');
Route::get('/artist/profile/{username}/edit', 'Artist\ArtistController@edit');
Route::put('/artist/profile/{username}', 'Artist\ArtistController@update')->name('artist.profile.edit.submit');

//Route::get('','')->name('');
Route::get('/music/create','MusicController@create')->name('music.create');
Route::get('/music','MusicController@index')->name('music.index');
Route::post('/music','MusicController@store')->name('music.store');
Route::get('/test','MusicController@test')->name('music.test');
Route::get('/musiclist','MusicController@musiclist')->name('music.musiclist');
Route::post('/like','LikeController@store')->name('like.store');
